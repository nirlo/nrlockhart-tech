+++
title="Kubernetes; Hard to Make it Easy"
date="2019-12-29"
tags=['Kubernetes', 'tech', 'cloud', 'CKAD', 'CKA', 'Docker']
+++

A couple of months ago, I completed my exam to become a Certified Kubernetes Application Developer. It was a slog to make it through the prep and complete the exam, but I have come through triumphantly. I can offically put `CKAD` on my exam and all over my Linkedin account. 

> What does it mean though?

Well, my original thought process behind completely the exam was to show compentency with a technology that I was using every day. The CKAD exam focuses on the application side of Kubernetes; deployments, persistent volumes, secrets, and observability of the application that you will deploy to the platform. I reveled in everything I was doing, so I wanted to show that I was capable.

There is a lot on the CKAD exam that I do not use on regular basis, but it helped me understand what was happening under the hood with the tools I was using, such as Helm. 

Fast forward after the Linux Foundations black friday sale for certifications, and I have bought the CKA exam. I'm getting my Tux mug soon!

Kubernetes is the way we, as an industry, are going to focus our work. Not because it is the new hotness and it's complicated and VPs like complication. No It's going to be everywhere because it is actually easy. It's fast to makes changes to you application, it's fast to scale. 

On Black Friday at Fullscript, our traffic was beyond any other day, well past tripling our normal traffic. As someone in DevOps, you would be expecting me to be running around like a chicken with it's head cut off, trying to put out fires in the database, on the application, in the networking.

None of that. We increases the number of pods running our application a few hours before the emails about our sale went out and watched our grafana dashboards, queitly sipping on coffee and avoiding the Customer Service teams as much as we could. 

Our Dev team had a few issues in flight they needed to fix, but those changes went out without any issue.

I've read `The Phoenix Project` recently (I want to go in depth about it another time), but I now the chaos that was Ops in the past. Black Friday would have been a week long event of overtime and no sleep. Instead, I sat on the sidelines and worked on some other projects and celebrated the fantastic sales.

Why? Because the processes enabled by Kubernetes made it possible to ensure the application didn't fall over. 

But all of that takes work. It takes forethought and ensuring high availability. 

As part of my preparation for the CKA exam, I went through `Kubernetes-the-hard-way` by Kelsey Hightower. I'm going to go through it a couple more times as it is extremely comprehensive. The scary part about it is not that it is the `hard way`, but because the `hard way` is actually not hard. There is not a lot that underlies the networking and the components are dead easy to know what they do.

Kubernetes makes work easier. I love working with Kubernetes not because of the technology, but because it makes work easy. 

I want to go through each portion of `kubernetes-the-hard-way` and explain each portion. That will be coming up in the new year!

