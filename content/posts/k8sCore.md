+++
title="Kubernetes Core Concepts"
date="2019-09-01"
tags=['Kubernetes', 'tech', 'cloud', 'CKAD', 'Docker']
+++
Today, I'll be going over the core concepts of Kubernetes. These are the basis of everything that you are going to do on Kubernetes. If you don't know how to work these basics, you're going to break your production, your director is going to come to your desk and ask you what the fuck did you just do. Well, you told them that you knew Kubernetes, so you gotta come up with some excuse about the configuration messing up and that you'll fix it before anyone knows what happened.

Most of what I cover here will ensure that you have at least 13% on the CKAD exam, so congratulations, you're almost there. 

You can find a lot of the code that I will be using for everything in this series on my gitlab profile. [https://gitlab.com/nirlo/k8s](https://gitlab.com/nirlo/k8s). I use Gitlab mostly because of the CI integration and image registry. Both features are free and well supported for any minor projects that you will work on. At a certain point, you will find that their CI misses some features that you could use, but that is a much further down the road problem when you're deploying multiple times a day.

So... What are 'Core Concepts'? They're the essentials that you need to run any application. They are: 

- Pods
- Deployments
- Service
- ReplicaSet
- ConfigMap
- Secrets
- DaemonSet
- Ingress
- Nodes & master

If you have the basic infrastructure set up, either through an Admin at you company or your company is using one of the big cloud providers, then these are going to your essentials. 

We'll need to know what kubernetes is at the basic level. That is going to be key going forward. 

Kubernetes is organized as a Central Manager and Worker nodes. The master Is the brain and the nervous system of kubernetes. It says jump and the workers will jump. It has components that ensure it stays operating. The kube-apiserver, the kube-scheduler, the kube-controller-manager, the cloud-controller-manager, and the etcd database. CoreDNS has become a staple of the system as well and other Daemonset components, but on the master, these are what we want to focus on.

The kube-apiserver is what you will use to communicate to kubernetes. Typically, it is going to be through kubectl, a local client that you will use to connect to the cluster.  All actions are validated through this component. It's the only connection to the etcd database. Everything In Kubernetes lives and dies by this process. It's that guy in your office that knows everything about the application and the one time he went on vacation, the entire server is on fire and your company just lost a week's worth of revenue. If this component breaks, you might as well kill the entire cluster because you will up shit's creek with no paddle.

The kube-scheduler takes requests from the API and decides how to best to use it. When you run `kubectl create pod -f <manifest>`, this command is sent to the API and then to the scheduler. The scheduler decides which node the pod will be hosted. It typically uses a algorithm to decide which node gets the pod based on available resources. I say typically because you can tag nodes and use the tag to tell Kubernetes that a pod must go on that node. If the scheduler cannot find a node to place the pod, it will remain in a pending state. 

The etcd databse is a b+tree key-value store. The state of the cluster, networking, and other persistent information is kept here, appended to the end of the store instead of modifying the value. Old copies of that data are marked for deletion by a later process. Multiple requests to update a value both travel via the kube-apiserver, then to the etcd is series. The first request updates the store. The next request would no longer have the same version number for the store and would reply with a 409 to the apiserver and requester. There are possible followers to the etcd, each deciding which one will be the master and decide who becomes the master in the event of the failure. 

The kube-controller-manager is a control loop daemon which interacts with the kube-apiserver to determine the state of the cluster. If the state does not match, the manager will contact the necessary controller to match the desired state. 

You might have noticed that I mentioned the word `manifest` before without much explaination. Well, it's just the fancy word that the CNCF decided to use for yaml files that describe your clusters.

Pods are the smallest managable piece of your kubernetes infrastructure. They are your application. They wrap a Docker container or containers and let you manage them all together. For web applications, your web worker or endpoint will ive as part of a pod. You can extend your application with logging, proxies, configuration reloading by adding containers to your pod specific to this end. All containers share a IP address and a namespace. 

Here, I have a pod that takes a simple go web server and deploys it as a pod. 

    apiVersion: v1
    kind: Pod
    metadata:
      labels:
        server: server
        k8s: k8s
        app: server
      name: goServer
    spec:
      containers:
      - name: goServer
        image: registry.gitlab.com/nirlo/k8s:latest
        ports:
        - containerPort: 9090

There are a few common key words that will be in every manifest that you write. 

- `apiVersion` is described for every manifest and it depends on the kind of manifest. It just tells k8s where to find the struct for what you want. It's arbitrary and changes on a whim
- `kind` is what the api will create. Here, I want to create a pod
- `metadata` is just that; names and labels that can be used to describe the object. Other information will be created when the object is instaniated.
- `spec` is the specification. Resources, VolumeMounts, taints and tolerations will be described
- `Containers` are your application and accompanying services. I'll go over why you may want to add extra containers when the time comes.

There is one container that every pod will always have, despite it never being seen. The Pause Contiainer. It's used during the startup of the pod to get the IP address and use the network namespace. 

Controllers are a series of watch-loops that manage the orchestration of a particular object, ensuring that the current state of the object matches the desired state. 

Deployments are the new default controller for pods and containers. It will ensure that resources like ip address and storage are availble and then launches a ReplicaSet. This will deploy and restart pods as necessary. Both of these will define the pods and the containers therein.

    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: daemonset
      server: server
      k8s: k8s
      app: server
    spec:
      replicas: 2
      selector:
        matchLabels:
          name: daemonset
      template:
        metadata:
          labels:
            server: server
            k8s: k8s
            app: server
          name: goServer
        spec:
          containers:
          - name: goServer
            image: registry.gitlab.com/nirlo/k8s:latest 
            ports:
            - containerPort: 9090

Jobs and Cronjobs are a special type of deployment. Instead of setting up the containers to run a longer standing application though, they run one off tasks such as database migrations. Once they complete their task, they close down the resources and containers. One done and out of the way. At this point, there is no reason to put a manifest for a job, it's close to a deployment.

Daemonsets are another special type deployment. The difference is that instead of a user determined number of pods, the number of pods deployed are set by the number of nodes in the cluster. Prometheus exporters and ElasticSearch beats would be deployed to each node to gather in the information needed for observability. Kube-proxy would be another system that would be deployed with a Daemonset.

    apiVersion: apps/v1
    kind: DaemonSet
    metadata:
      name: daemonset
      server: server
      k8s: k8s
      app: server
    spec:
      selector:
        matchLabels:
          name: daemonset
      template:
        metadata:
          labels:
            server: server
            k8s: k8s
            app: server
          name: goServer
        spec:
          containers:
          - name: goServer
            image: registry.gitlab.com/nirlo/k8s:latest
            ports:
            - containerPort: 9090

Connecting all these deployments with the traffic that they need is the job of Services. Each service is a microservice handling a particular bit of traffic. When you have multiple pods that run the same application, you need to ensure that each pod does not get overloaded. So, a service will route the traffic in a round robin style, distrubing the traffic such as a Nodeport or a loadbalancer.

    apiVersion: v1
    kind: service
    metadata:
      name: serverService
    spec:
      selector:
        app: server
      ports:
        - protocol: TCP
          port: 9090
          targetPort: "9090"

Ingresses will route traffic to the service and the service will route to the application pod that has not been overloaded as designated by the service.

    apiVersion: extensions/v1beta1
    kind: Ingress
    metadata:
      name: server-ingress
      annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /
    spec:
      rules:
      - http:
        paths:
        - path: /
          backend:
            serviceName: serverService
            servicePort: 9090

When dealing with the configuration, you have two means of working with values. You can place the configuration values in a Configmap. These can then then be injested by the application pods. You can also pass secret values, encrypt the values like passwords and secret keys in a Secret. These are encrypted once they created in the cluster. 

    apiVersion: v1
    kind: ConfigMap
    metadata:
      name: server-config
    data:
      server.properties: /
        serverPort=9090

These are the basics for dealing with applications that will be deployed to Kubernetes.

I'll be going into more depth as this series goes along. I'm being selfish and using this as my own study for the CKAD exam. So, until next time!