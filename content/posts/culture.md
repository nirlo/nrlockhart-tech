+++
title = "Culture"
date = "2019-07-19"
tags = ['tech', 'culture', 'workplace', 'DevOps']
+++

What makes a workplace worth while? What differentiates a great workplace from a piss poor office?

Culture is what makes the difference. 

Knowing that you are a part of something ensures that you feel encouraged. You want to contribute, to give back and give as much as you can, becoming part of the process. 

Yea, that's good and all, and definitely the most corpo-spoke that I have ever spewed forth from my fingers. I can feel the wool growing on my back.

BAAAAAAHHHH :sheep:

But seriously. If you are going to spending a lot of time at a workplace, you need a reason to do it. Avoiding the drudgery is important. The worst thing you could be doing is going to place you hate to be and doing something you hate. 

For Software Developers, it can be difficult. There are times that you will create a feature for the main application or create a new process, but it has to be back logged and pushed out to the next next major release. 

I've been there. I worked on an automation process to decrease the amount of human interation needed to install virtual machines on bare metal machines. I worked on this during an entire Coop term. It was built using Ansible, TCL, and few bach scripts. I wanted the process to make the client life easier and ensure we would have fewer clients coming back with tickets and issues. The expectation was that the change would go out with the next major platform release. 

The end of my term came and my changes were pushed to the next release. That wasn't going out for another year or so. 

This shop was definitely on the more traditional side of software development, with some agile thrown in for good measure. 

I didn't feel a part of the process, I felt like my work was never going to see the light of day. I could guess that my productivity would have dropped over time if I had of worked in a shop like that. 

This here is the reason I extoll DevOps. It creates an atmosphere of engagement and interest in the process. 

Software developers become part of the process and engage in it more than waterfall, agile, or anything else. 

No Silos of knowledge, no team isolation, no slow down to wait on a particular release schedule.

Adding DevOps principals to a dev shop ensures you have less time at the pingpong table and more time discussing the issues that are actually affecting the product going out. 

Engagement and collaboration should be high at all times.
