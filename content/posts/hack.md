+++
title="Thoughts on The Great Hack"
date="2019-07-27"
tags=['data', 'tech', 'ethics', 'facebook', 'information', 'psyops']
draft=true
+++

> "If it's in the right hands, we can change the world for the better"

This is something that I think a lot of people will think about when they start to look into uses of technology. It was a statement by my partner while we watched *The Great Hack*, the new documentary on Netflix, detailing the fall of Cambridge Analytica. 