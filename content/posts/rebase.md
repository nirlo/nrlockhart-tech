+++ 
title = "GitSome: Rebase patience"
draft = "true"
date = "2019-07-18"
tags = ['GitSome', 'Rebase', 'How-to']
+++

Once you start working with Git for your code, you can start to get into a routine, your workflow, the way that you write code and the way that you make changes to your code. Loop; Working on an issue, making a new feature, refactoring the interns code, adding tests; end. You know the usual commands for git, you can manage a merge conflict without too much hassle. 