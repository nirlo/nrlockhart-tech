---
title: "Git Some"
date: "2019-06-30"
tags: 
- git
- software

---

# Git Some

Git has a strange place in software development. Walk into any dev shop and I can guarantee that they use git. If not, walk out as fast as you can and hope you or one of your friends don’t get called from their recruiter. Sure, subversion is still in use in some shops, it may have it uses… but again, walk away at a brisk enough pace. 


The thing with git though, not many actually know what it is doing. Most devs, their days consist of creating their feature branch and pushing it to the remote repo their employer set up. If something goes wrong, everyone runs to the nearest sysadmin or DevOps practicing employee. The fear in their eyes as the words `merge conflict` flashes across their screen. “What do I? Do I accept the incoming changes?” they plead with their peer. A moment later and a few keystrokes and the issue is gone. The Sysadmin-esque person has solved their problem, explains what happened, but both parties understand that within the next week the dev will be back with a similar issue and a similar log. The cycle endures.


I took a two year diploma program for Computer Programming. We learned fantastic things like “How to run a SQL Query! Now repeat that for two more courses”, “Make a DOA in PHP! Don’t worry about what it is, just do it!”, “Learn a programming language of your choice! Spend 90% of your time writing reports!”, and my personal favorite, “Copy code from the internet! It’s fine, it’s not plagiarism! let’s play Kahoot!” Not one of the courses for that program was for version control. I’ve spoken with my colleagues as well, and they have the same experience. No courses on version control. 


The most ubiquitous tool in our industry, and most of us have to learn it on our own. Some workplaces understand this and will do workshops. But often, the on the job learning for version control is during pair programming or the word of mouth. 


It’s become our oral tradition. Passed down from the elders, blessing the juniors with their wise commands and dictates. 


Oral histories fail eventually and no one in the office truly knows what `--no-ff` but uses it because it’s what Gitlab tells them to use when it refuses to merge automatically. Let’s write this oral history down to ensure that we know what git is truly doing. 


I will not talk about the commands under the “collaboration” heading in the help page. Most devs know what these are doing. Run `git --help` if you don’t know what these commands are, I’m sure it’s the first time you ran this command and reading help pages is scary. 


I want to share some of the learning that I have from trundling along with git. I want there to be an understanding of how to use rebase properly, how `git reflog` is going to save you time.


I'll add new additions to Git Some as I go along and find new tricks that can help your git flow.