---
title: Introduction
tags:
- introduction
date: "2019-06-30"
---
# Introduction

In almost every profession, there is a need for communication. Your team needs to know what you are working on so they can work around it. Knowledge needs to be shared from one person to the next. In software development, there is some knowledge that is shared, but it can be spread by word of mouth. Or you end up rubber ducking your way through a problem and don't commincate what the solution was. 

None of the above is what this blog is going to be about. 



Well, slight lie. Mostly, this is going to be the rubber duck of my world. The propblems that I want to talk through, that I want to express.

I'll have a weekly post on something random that I am interested in; something to do with the MCU, pop culture, something that I have read or watched over the last week. 

As I find problems with software, I'll write them down here and express what happened and the fix that I had.

Oh, I forgot to mention. I am a DevOps Engineer at a company called Fullscript. I mostly work with Kubernetes, but a majority of my job is finding new ways of describing infrastructure to developers and beyond. Metrics turned into visualizations that can be easily parsed, or articles on how to use a new piece of the system. Teaching not only developers but analysts how to use some of the more complicated systems that exist in the DevOps sphere. Mostly Kubernetes, because that is a beast that takes some getting used to in it's structure. 